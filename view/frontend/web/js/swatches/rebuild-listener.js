define(["jquery"], function ($) {
    "use strict";

    $.mixin("SwatchRenderer", {
        _Rebuild: function (original) {
            original();

            let products = this._CalcProducts();
            if (
                products.length === 1 &&
                typeof window.mstGtmProductVariants != "undefined"
            ) {
                if (
                    typeof window.mstGtmProductVariants[products[0]] !=
                    "undefined"
                ) {
                    _.each(
                        window.mstGtmProductVariants[products[0]],
                        function (item) {
                            var gtmStorageKey = "";
                            if (typeof item.gtm_id != "undefined") {
                                gtmStorageKey = item.gtm_id;
                            }

                            if (item && typeof item.gtm_id != "undefined") {
                                window.dataLayer.push({ ecommerce: null });

                                if (typeof gtag != "undefined") {
                                    gtag(item[0], item[1], item[2]);
                                } else {
                                    var formatedObj = {};
                                    formatedObj[item[0]] = item[1];
                                    formatedObj["ecommerce"] = item[2];

                                    window.dataLayer.push(formatedObj);
                                }
                            }
                        }
                    );
                }
            }

            return this;
        },
    });
});
