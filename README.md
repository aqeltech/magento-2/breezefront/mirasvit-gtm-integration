# Mirasvit GTM Integration

## Description

This extension does its best to integrate all storefront features of Google Tag Manager extension form Mirasvit vendor.

## Required patches

`mirasvit/module-gtm/src/GoogleTagManager/view/frontend/web/js/layer.js`

```diff
@@ -10,9 +10,9 @@
         initialize: function () {
             this._super();
 
-            const gtm = customerData.get('gtm');
+            const gtm = $.customerData.get('gtm');
 
-            customerData.reload(['gtm'], false);
+            $.customerData.reload(['gtm'], false);
 
             gtm.subscribe(this.onUpdate);
         },
```

`mirasvit/module-gtm/src/GoogleTagManager/view/frontend/web/js/event/addtocart.js`

```diff
@@ -10,9 +10,9 @@
         initialize: function () {
             this._super();
 
-            const tm = customerData.get('mst-tm-addtocart');
+            const tm = $.customerData.get('mst-tm-addtocart');
 
-            customerData.reload(['mst-tm-addtocart'], false);
+            $.customerData.reload(['mst-tm-addtocart'], false);
 
             tm.subscribe(this.onUpdate);
         },
```

## Installation

```bash
composer require aqeltech/module-breeze-mirasvit-gtm
bin/magento module:enable AQELTech_BreezeMirasvitGTM
```
